package basic.sign.up.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import basic.sign.up.common.Response;

@RestController
public class HealthController {

	Logger logger = LoggerFactory.getLogger(HealthController.class);

	@RequestMapping("/health")
	public Response index() {
		return new Response("Aplicacion arriba y funcionando!");
	}

}