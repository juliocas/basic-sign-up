package basic.sign.up.controllers;

import basic.sign.up.common.Response;
import basic.sign.up.domain.User;
import basic.sign.up.service.api.IUserService;
import basic.sign.up.service.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/user")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public Response getUsers() {
        List<UserDto> users;
        try {
            logger.info("Consultando usuarios");
            users = userService.getUsers();
        } catch (Exception e) {
            logger.error("Error en consulta de usuarios: " + e.getMessage());
            return new Response(e.getMessage());
        }
        String message = "Consulta realizada con exito";
        logger.info(message);
        return new Response(message, users);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Response getUser(@PathVariable String id) {
        Optional<User> user;
        try {
            logger.info("Consultando usuario: " + id);
            user = userService.getUser(id);
        } catch (Exception e) {
            logger.error("Error en consulta de usuario " + id + " : " + e.getMessage());
            return new Response(e.getMessage());
        }
        String message = "Consulta realizada con exito";
        logger.info(message);
        return new Response(message, user);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Response insertUser(@RequestBody UserDto userDto, @RequestHeader(name = "authorization") String token) {
        User user;
        try {
            logger.info("Creando usuario: " + userDto.getEmail());
            user = userService.createUser(userDto, token);
        } catch (Exception e) {
            logger.error("Error en creación de usuario : " + e.getMessage());
            return new Response(e.getMessage(), e.toString());
        }
        String message = "Usuario " + userDto.getEmail() + " creado con exito";
        logger.info(message);
        return new Response(message, user);
    }
}