package basic.sign.up.common;

public class Response {
    private String mensaje;
    private Object data;

    public Response() {
    }

    public Response(String mensaje) {
        this.mensaje = mensaje;
    }

    public Response(Object data) {
        this.data = data;
    }

    public Response(String mensaje, Object data) {
        this.mensaje = mensaje;
        this.data = data;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
