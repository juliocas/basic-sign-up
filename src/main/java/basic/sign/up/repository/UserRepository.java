package basic.sign.up.repository;

import basic.sign.up.domain.User;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {

    List<User> findByEmail(String email);
}
