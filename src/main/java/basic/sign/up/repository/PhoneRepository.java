package basic.sign.up.repository;

import basic.sign.up.domain.Phone;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhoneRepository extends CrudRepository<Phone, String> {

    List<Phone> findByUserId(String user_id);
}
