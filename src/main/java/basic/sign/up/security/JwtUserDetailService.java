package basic.sign.up.security;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailService implements UserDetailsService {

    Logger logger = LoggerFactory.getLogger(JwtUserDetailService.class);

    private static final String JWT_USER = "root";
    private static final String JWT_PASSWORD = "$2a$10$HaV/Dms6x7OBUIKBMWfHE.qBgWjfvFzXoAcg.7p9Q/KBwx9wLC7N.";

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (JWT_USER.equals(username)) {
            logger.info("Usuario token valido");
            return new User(JWT_USER, JWT_PASSWORD, new ArrayList<>());
        } else {
            logger.info("Usuario token invalido");
            throw new UsernameNotFoundException("Usuario: " + username + " no valido");
        }
    }
}

