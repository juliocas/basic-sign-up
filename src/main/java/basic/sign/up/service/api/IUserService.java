package basic.sign.up.service.api;

import basic.sign.up.domain.User;
import basic.sign.up.service.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    Optional<User> getUser(String id);

    List<UserDto> getUsers();

    User createUser(UserDto userDto, String token) throws Exception;
}
