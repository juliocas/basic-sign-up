package basic.sign.up.service;

import basic.sign.up.domain.Phone;
import basic.sign.up.domain.User;
import basic.sign.up.repository.PhoneRepository;
import basic.sign.up.repository.UserRepository;
import basic.sign.up.service.api.IUserService;
import basic.sign.up.service.dto.UserDto;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private Mapper mapper;

    public UserService() {
    }

    @Override
    public List<UserDto> getUsers() {
        List<UserDto> userDtoList = new ArrayList<UserDto>();
        userRepository.findAll().forEach(userEntity -> {
            userDtoList.add(mapper.map(userEntity, UserDto.class));
        });
        return userDtoList;
    }

    @Override
    public Optional<User> getUser(String id) {
        return userRepository.findById(id);
    }

    @Override
    public User createUser(UserDto userDto, String token) throws Exception {
        try {
            if (!userRepository.findByEmail(userDto.getEmail()).isEmpty()) {
                throw new Error("El correo ya está registrado");
            }

            userDto.setId(UUID.randomUUID().toString());
            userDto.setCreated(new Date());
            userDto.setLast_login(new Date());
            userDto.setModified(new Date());
            userDto.setIsactive(true);
            userDto.setToken(token);

            for (Phone phone : userDto.getPhones()) {
                phone.setId(UUID.randomUUID().toString());
                phone.setUserId(userDto.getId());
            }

            User user = mapper.map(userDto, User.class);
            return userRepository.save(user);
        } catch (Error e) {
            throw new Exception(e.getMessage());
        }
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void setMapper(Mapper mapper) {
        this.mapper = mapper;
    }
}
