package basic.sign.up.service.dto;

import basic.sign.up.domain.Phone;

import java.util.*;

public class UserDto {

    private String id;
    private String name;
    private String email;
    private String password;
    private List<Phone> phones;
    private Date created;
    private Date modified;
    private Date last_login;
    private Boolean isactive;
    private String token;

    public UserDto() {
        super();
    }

    public UserDto(String id, String name, String email, String password, String token, List<Phone> phones) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.phones = phones;
        this.created = new Date();
        this.modified = new Date();
        this.modified = new Date();
        this.token = token;
        this.isactive = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDto)) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(getId(), userDto.getId()) &&
                Objects.equals(getName(), userDto.getName()) &&
                Objects.equals(getEmail(), userDto.getEmail()) &&
                Objects.equals(getPassword(), userDto.getPassword()) &&
                Objects.equals(getPhones(), userDto.getPhones()) &&
                Objects.equals(getCreated(), userDto.getCreated()) &&
                Objects.equals(getModified(), userDto.getModified()) &&
                Objects.equals(getLast_login(), userDto.getLast_login()) &&
                Objects.equals(getIsactive(), userDto.getIsactive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getPassword(), getPhones(), getCreated(), getModified(), getLast_login(), getIsactive());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }
}
