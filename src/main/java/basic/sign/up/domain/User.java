package basic.sign.up.domain;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Pattern(regexp = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$")
    @Column(name = "email")
    private String email;

    @Pattern(regexp = "^(\\b[A-Z])+([a-z])+([0-9]{2})?$")
    @Column(name = "password")
    private String password;

    @Column(name = "created")
    private Date created;

    @Column(name = "modified")
    private Date modified;

    @Column(name = "last_login")
    private Date last_login;

    @Column(name = "token")
    private String token;

    @Column(name = "isactive")
    private Boolean isactive;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            targetEntity = Phone.class,
            mappedBy = "userId")
    private List<Phone> phones;

    public User() {
    }

    public User(String id, String name, String email, String password, String token) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.created = new Date();
        this.modified = new Date();
        this.modified = new Date();
        this.token = token;
        this.isactive = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId()) &&
                Objects.equals(getName(), user.getName()) &&
                Objects.equals(getEmail(), user.getEmail()) &&
                Objects.equals(getPassword(), user.getPassword()) &&
                Objects.equals(getCreated(), user.getCreated()) &&
                Objects.equals(getModified(), user.getModified()) &&
                Objects.equals(getLast_login(), user.getLast_login()) &&
                Objects.equals(getIsactive(), user.getIsactive()) &&
                Objects.equals(getPhones(), user.getPhones());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getPassword(), getCreated(), getModified(), getLast_login(), getIsactive(), getPhones());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getLast_login() {
        return last_login;
    }

    public void setLast_login(Date last_login) {
        this.last_login = last_login;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }
}