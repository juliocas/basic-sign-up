package basic.sign.up.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "phone")
public class Phone implements Serializable {

    @Id
    private String id;

    private String userId;

    private Integer countrycode;

    private Integer citycode;

    private String number;


    public Phone() {
    }

    public Phone(String id, String userId, String number, Integer citycode, Integer countrycode) {
        this.id = id;
        this.userId = userId;
        this.number = number;
        this.citycode = citycode;
        this.countrycode = countrycode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Phone)) return false;
        Phone phone = (Phone) o;
        return Objects.equals(getId(), phone.getId()) &&
                Objects.equals(getUserId(), phone.getUserId()) &&
                Objects.equals(getCountrycode(), phone.getCountrycode()) &&
                Objects.equals(getCitycode(), phone.getCitycode()) &&
                Objects.equals(getNumber(), phone.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserId(), getCountrycode(), getCitycode(), getNumber());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userid) {
        this.userId = userid;
    }

    public Integer getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(Integer countrycode) {
        this.countrycode = countrycode;
    }

    public Integer getCitycode() {
        return citycode;
    }

    public void setCitycode(Integer citycode) {
        this.citycode = citycode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}