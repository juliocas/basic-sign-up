package basic.sign.up.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import basic.sign.up.controllers.HealthController;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(HealthController.class)
@AutoConfigureMockMvc
public class HealthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void healthMessageUnauthorizedFromService() throws Exception {
        this.mockMvc.perform(get("/health").header("authorization", "Bearer " + "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyb290IiwiZXhwIjoxNTgxOTU1NTI5LCJpYXQiOjE1ODE5NTUyMjl9.lNu6WKVPoWHWw2rurcGFv2SbM1oTmi3yeYKBTFT3GCG56zAfIAjXqwKPIY1BIOuABmkP90hGdcjwdJes4zXDWw"))

                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void healthMessageAuthorizedFromService() throws Exception {
        this.mockMvc.perform(get("/health")
                .with(jwt()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .json("{\"mensaje\":\"Aplicacion arriba y funcionando!\",\"data\":null}")
                );
    }
}

