package basic.sign.up.controller;

import basic.sign.up.controllers.UserController;
import basic.sign.up.domain.Phone;
import basic.sign.up.domain.User;
import basic.sign.up.service.UserService;
import basic.sign.up.service.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @Test
    public void usersGetFromService() throws Exception {
        List<UserDto> usersMock = new ArrayList<UserDto>();
        when(service.getUsers()).thenReturn(usersMock);
        this.mockMvc.perform(get("/user/")
                .with(jwt()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"mensaje\":\"Consulta realizada con exito\",\"data\":[]}"));
    }

    @Test
    public void userGetFromService() throws Exception {
        User user = new User("abc", "name", "email1@email.com", "pass", "token");
        when(service.getUser("abc")).thenReturn(Optional.of(user));
        this.mockMvc.perform(get("/user/abc")
                .with(jwt()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Consulta realizada con exito")))
                .andExpect(content().string(containsString("\"id\":\"abc\"")));
    }

    @Test
    public void userPostFromService() throws Exception {
        User userMock = new User("abc", "name", "email1@email.com", "pass", "token");
        UserDto mockDto = new UserDto("abc", "name", "email1@email.com", "pass", "token", new ArrayList<Phone>());
        when(service.createUser(mockDto, "token")).thenReturn(userMock);
        this.mockMvc.perform(post("/user/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(mockDto))
                .header("Authorization", "token")
                .with(jwt()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Usuario email1@email.com creado con exito")))
                .andExpect(content().string(containsString("\"id\":\"abc\"")));
    }
}
