package basic.sign.up.controller;

import basic.sign.up.security.JwtAuthenticationController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(JwtAuthenticationController.class)
@AutoConfigureMockMvc
public class JwtAuthenticationTestController {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void obtainAccessOkToken() throws Exception {
        String username = "root";
        String password = "root";
        this.mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void obtainAccessUnauthorizedToken() throws Exception {
        String username = "invalid";
        String password = "root";
        this.mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

}
