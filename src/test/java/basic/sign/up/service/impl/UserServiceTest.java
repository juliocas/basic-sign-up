package basic.sign.up.service.impl;

import basic.sign.up.domain.Phone;
import basic.sign.up.domain.User;
import basic.sign.up.repository.UserRepository;
import basic.sign.up.service.UserService;
import basic.sign.up.service.dto.UserDto;
import org.dozer.Mapper;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
public class UserServiceTest {

    @Test public void testGetUserMethod() {
        User userMock = new User("abc", "name", "email@email.com", "pass", "token");
        UserRepository serviceMock = mock(UserRepository.class);
        when(serviceMock.findById("abc")).thenReturn(Optional.of(userMock));

        UserService classTest = new UserService();
        classTest.setUserRepository(serviceMock);
        String id = "abc";
        Optional<User> user = classTest.getUser(id);

        assertEquals("Method should return user successfully", user.get().getId(), id);
    }

    @Test public void testGetUsersMethod() {
        List<User> usersMock = new ArrayList<>();
        usersMock.add(new User("abc", "name", "email1@email.com", "pass", "token"));
        usersMock.add(new User("abd", "name", "email2@email.com", "pass", "token"));

        UserDto mockDto = mock(UserDto.class);

        UserRepository serviceMock = mock(UserRepository.class);
        when(serviceMock.findAll()).thenReturn(usersMock);
        Mapper mapperMock = mock(Mapper.class);
        when(mapperMock.map(usersMock.get(0), UserDto.class)).thenReturn(mockDto);
        when(mapperMock.map(usersMock.get(1), UserDto.class)).thenReturn(mockDto);

        UserService classTest = new UserService();
        classTest.setUserRepository(serviceMock);
        classTest.setMapper(mapperMock);
        List<UserDto> users = classTest.getUsers();

        assertEquals("Method should return all users successfully", users.size(), usersMock.size());
    }

    @Test public void testCreateUserEmailRegisteredMethod() throws Exception {
        User userMock = new User("abc", "name", "email1@email.com", "pass", "token");
        List<User> userList = new ArrayList<User>();
        userList.add(userMock);
        UserDto mockDto = new UserDto("abc", "name", "email1@email.com", "pass", "token", new ArrayList<Phone>());

        UserRepository serviceMock = mock(UserRepository.class);
        when(serviceMock.findByEmail("email1@email.com")).thenReturn(userList);

        UserService classTest = new UserService();
        classTest.setUserRepository(serviceMock);

        try {
            classTest.createUser(mockDto, "token");
        } catch (Exception e) {
            assertEquals("Method should return El correo ya está registrado", e.getMessage(), "El correo ya está registrado");
        }
    }

    @Test public void testCreateUserMethod() throws Exception {
        User userMock1 = new User("abc", "name", "email1@email.com", "pass","token");
        User userMock2 = new User("abc", "name", "email2@email.com", "pass", "token");
        List<User> userList = new ArrayList<User>();
        userList.add(userMock1);
        UserDto mockDto = new UserDto("abc", "name", "email2@email.com", "pass", "token", new ArrayList<Phone>());

        UserRepository serviceMock = mock(UserRepository.class);
        when(serviceMock.findByEmail("email1@email.com")).thenReturn(userList);
        when(serviceMock.save(userMock2)).thenReturn(userMock2);
        Mapper mapperMock = mock(Mapper.class);
        when(mapperMock.map(mockDto, User.class)).thenReturn(userMock2);

        UserService classTest = new UserService();
        classTest.setUserRepository(serviceMock);
        classTest.setMapper(mapperMock);

        User user = classTest.createUser(mockDto, "token");

        assertEquals("Method should return user successfully", user.getEmail(),mockDto.getEmail());
    }
}
