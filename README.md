
# Basic User Sign Up

Aplicación para creación de Usuarios.

#### Recursos:
Diagrama de componentes, Diagrama de secuencia y postman collection en carpeta main/resources.

#### Para levantar la aplicación en ambiente local

- Clone repositorio alojado en: 
- Correr ./gradlew bootRun en la terminal

#### Para acceder a la consola de administración
- http://localhost:8080/console
- Credenciales:
```bash
JDBC URL: jdbc:h2:mem:users
User: root
Password: root
```

#### Para generar token de acceso
- Obtener token de acceso con el endpoint authenticate:
```bash
curl -X POST \
  http://localhost:8080/authenticate \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 5ed9ee88-f7db-338d-43ac-6e4760fe8a43' \
  -d '{
	"username": "root",
	"password": "root"
}'
```
En adelante, agregar el token de acceso como cabecera Bearer Token a la petición.

#### Para crear usuarios:

```bash
curl -X POST \
  http://localhost:8080/user \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'authorization: Bearer {{access_token}}' \
  -d '{
	"name": "Julio Castillo",
	"email": "julio@google.com",
	"password": "Qwerty43",
	"hola": "",
	"phones": [
		{
			"number": "12345678",
			"citycode": "1",
			"countrycode": "57"
		},
		{
			"number": "12345678",
			"citycode": "2",
			"countrycode": "57"
		}
	]
}'
```

#### Para consultar usuarios:

```bash
curl -X GET \
  http://localhost:8080/user \
  -H 'cache-control: no-cache' \
  -H 'authorization: Bearer {{access_token}}' \
```

#### Para consultar un usuario:

```bash
curl -X GET \
  http://localhost:8080/user/a93dcd3a-0726-44b2-a5f9-676ec4589419 \
  -H 'cache-control: no-cache' \
  -H 'authorization: Bearer {{access_token}}' \
```
